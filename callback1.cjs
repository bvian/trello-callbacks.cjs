const fs = require("fs");

function getBoardInfo(boardId, callbackFunction) {

    setTimeout(() => {
        if (typeof boardId !== 'string') {
            const err = new Error('Parameter is not passed correctly');
            callbackFunction(err);
        } else {

            const boardPath = `${__dirname}/boards.json`;

            fs.readFile(boardPath, "utf-8", (err, data) => {

                if (err) {
                    callbackFunction(err);
                } else {

                    const boards = JSON.parse(data);

                    const currentBoard = boards.find((board) => {
                        return board.id === boardId;
                    });

                    if (currentBoard === undefined) {
                        const err = new Error(`board Id ${boardId} does not exists.`);
                        callbackFunction(err);
                    } else {
                        callbackFunction(null, currentBoard);
                    }
                }
            });
        }
    }, 2 * 1000);
}

module.exports = getBoardInfo;