const fs = require("fs");

function getListInfoFromCards(listId, callbackFunction) {

    setTimeout(() => {

        if (typeof listId !== 'string') {
            const err = new Error('Parameter is not passed correctly');
            callbackFunction(err);
        } else {

            const cardPath = `${__dirname}/cards.json`;

            fs.readFile(cardPath, "utf-8", (err, data) => {

                const cards = JSON.parse(data);

                if (err) {
                    callbackFunction(err);
                } else {

                    if (cards[listId] === undefined) {
                        const err = new Error(`list Id ${listId} does not exists in cards.`);
                        callbackFunction(err);
                    } else {
                        callbackFunction(null, cards[listId]);
                    }
                }
            });
        }

    }, 2 * 1000);
}

module.exports = getListInfoFromCards;