const getBoardInfo = require('../callback1.cjs');

const boardId = "mcu453ed";

getBoardInfo(boardId, (err, boardInfo) => {
    if (err) {
        console.error(err);
    } else {
        console.log(boardInfo);
    }
});