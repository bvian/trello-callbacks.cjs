const fs = require("fs");

function getBoardInfoFromLists(boardId, callbackFunction) {

    setTimeout(() => {

        if (typeof boardId !== 'string') {
            const err = new Error('Parameter is not passed correctly');
            callbackFunction(err);
        } else {

            const listPath = `${__dirname}/lists.json`;

            fs.readFile(listPath, "utf-8", (err, data) => {

                if (err) {
                    callbackFunction(err);
                } else {

                    const lists = JSON.parse(data);

                    if (lists[boardId] === undefined) {
                        const err = new Error(`board Id ${boardId} does not exists in lists.`);
                        callbackFunction(err);
                    } else {
                        callbackFunction(null, lists[boardId]);
                    }
                }
            });
        }

    }, 2 * 1000);
}

module.exports = getBoardInfoFromLists;