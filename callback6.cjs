const fs = require("fs");

const getBoardInfo = require('./callback1.cjs');
const getBoardInfoFromLists = require('./callback2.cjs');
const getListInfoFromCards = require('./callback3.cjs');

function getBoardListAndCardsFromAllLists(boardName) {

    setTimeout(() => {

        const boardPath = `${__dirname}/boards.json`;

        fs.readFile(boardPath, "utf-8", (err, data) => {

            const boards = JSON.parse(data);

            if (err) {
                console.error(err);
            } else {

                const boardId = boards.reduce((boardIdFromName, board) => {
                    if (board.name.includes(boardName)) {
                        boardIdFromName = board.id;
                    }

                    return boardIdFromName;
                }, "");

                getBoardInfo(boardId, (err, boardInfo) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(boardInfo);

                        getBoardInfoFromLists(boardId, (err, dataFromLists) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log(dataFromLists);

                                function recursiveGetListInfo(index) {

                                    if (index === dataFromLists.length) {
                                        return;
                                    }

                                    getListInfoFromCards(dataFromLists[index].id, (err, dataFromCards) => {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            console.log(dataFromCards);
                                        }
                                    });

                                    recursiveGetListInfo(index + 1);
                                }
                                recursiveGetListInfo(0);
                            }
                        });
                    }
                });
            }
        });

    }, 2 * 1000);
}

module.exports = getBoardListAndCardsFromAllLists;